var fs = require('fs'); 
var parse = require('csv-parse');
var iconv = require('iconv-lite');
var mysql = require('mysql'); 

// TRUNCATE klassen;TRUNCATE kurswahl;TRUNCATE lehrer;TRUNCATE schueler;TRUNCATE sgruppen; TRUNCATE unterricht;

// d:\xampp\mysql\bin\mysql.exe -u root untis21 --default-character-set=utf8 < data_out\kurswahl.sql

function generateInserts(tablename,csvdata) {
	let data=[];
	for (let r in csvdata) {
		let row=csvdata[r];
		let ins='INSERT INTO '+tablename+' VALUES (';
		for (let cell in row) {
			ins+="'"+row[cell]+"',"
		}
		ins=ins.substring(0,ins.length-1)+");\r\n";
		data.push(ins);
	}
	return data;
}

function convert(infile,tablename,con) {
	let csvData=[];
	return new Promise( (resolve, reject) => {
		fs.createReadStream('data_in/'+infile)
			.pipe(iconv.decodeStream('win1250'))
			.pipe(iconv.encodeStream('utf8'))
			.pipe(parse({delimiter: ';'}))
			.on('data', (csvrow) => {			
				//do something with csvrow
				csvData.push(csvrow);        
			})
			.on('end',() => {
				//do something with csvData
				console.log('read '+infile);
				resolve(generateInserts(tablename,csvData));
			})
			.on('error', err => reject(err))
		}
	);
}

function runQueries(queries,con) {
	let promises=[];
	for (let i=0;i<queries.length;i++) {
		promises.push(query(queries[i],con));
	}
	let res=Promise.all(promises);
	return res;
}

function query(sql,con) {
	return new Promise( ( resolve, reject ) => {
        con.query( sql, ( err, rows ) => {
		console.log(sql);
        if ( err )
            return reject( err );
        resolve( rows );
        } );
    } );;
}

function clearDB(con) {
	let clearCmd=['TRUNCATE klassen',
		'TRUNCATE kurswahl',
		'TRUNCATE lehrer',
		'TRUNCATE schueler',
		'TRUNCATE sgruppen',
		'TRUNCATE unterricht'
	];
	return runQueries(clearCmd,con);
}

// ========== MAIN ===========

var lastlog=0;

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "untis22"
});

con.connect(async function(err) {
	if (err) throw err;
	console.log('Connected, clearing...');
	await clearDB(con);

	console.log('populating DB...');
	
	try {
		await convert('GPU004.TXT','lehrer',con)
		.then (ins => runQueries(ins,con));
	} catch (error) {
		console.log ('error'+error);
	}
	
	try {
		await convert('GPU002.TXT','unterricht',con)
		.then (ins => runQueries(ins,con));
	} catch (error) {
		console.log ('error'+error);
	}
	
	try {
		await convert('GPU003.TXT','klassen',con)
		.then (ins => runQueries(ins,con));
	} catch (error) {
		console.log ('error'+error);
	}

	try {
		await convert('GPU010.TXT','schueler',con)
		.then (ins => runQueries(ins,con));
	} catch (error) {
		console.log ('error'+error);
	}
	
	try {
		await convert('GPU015.TXT','kurswahl',con)
		.then (ins => runQueries(ins,con));
	} catch (error) {
		console.log ('error'+error);
	}

	con.end();
	process.exit(0);
	
});

