const sqlite3 = require('sqlite3').verbose();
const fs = require('fs'); 

function readsql(sqlfile) {
	let sqlcmd=fs.readFileSync(sqlfile,{encoding:'utf8', flag:'r'});
	sqlcmd=sqlcmd.split(';');
	const regex_cr = /\r?\n|\r/g;
	const regex_def = / DEFAULT NULL/g;
	for (let i=0;i<sqlcmd.length;i++) {
		sqlcmd[i]=sqlcmd[i].substring(sqlcmd[i].indexOf('CREATE'),sqlcmd[i].length);
		sqlcmd[i]=sqlcmd[i].substring(sqlcmd[i].indexOf('INSERT'),sqlcmd[i].length);
		sqlcmd[i]=sqlcmd[i].replace(regex_cr,'');
		sqlcmd[i]=sqlcmd[i].replace(regex_def,'');
	}
	return sqlcmd;
}

var cmds=readsql('untis21.sql');

function query(sql,con) {
	return new Promise( ( resolve, reject ) => {
        con.run( sql, ( err, rows ) => {
		console.log(sql);
        if ( err )
            return reject( err );
        resolve( rows );
        } );
    } );;
}

function runQueries(queries,con) {
	let promises=[];
	for (let i=0;i<queries.length;i++) {
		promises.push(query(queries[i],con));
	}
	let res=Promise.all(promises);
	return res;
}

let db = new sqlite3.Database('untis.db', async (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('connect ok');

  await runQueries(cmds,db);

});