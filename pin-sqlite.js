var fs = require('fs'); 
var parse = require('csv-parse');
var iconv = require('iconv-lite');

import sqlite3 from 'sqlite3'
import { open } from 'sqlite'


function generateInserts(tablename,csvdata) {
	let data=[];
	for (let r in csvdata) {
		let row=csvdata[r];
		let ins='INSERT INTO '+tablename+' VALUES (';
		for (let cell in row) {
			ins+="'"+row[cell]+"',"
		}
		ins=ins.substring(0,ins.length-1)+");\r\n";
		data.push(ins);
	}
	return data;
}

function convert(infile,tablename,con) {
	let csvData=[];
	return new Promise( (resolve, reject) => {
		fs.createReadStream('data_in/'+infile)
			.pipe(iconv.decodeStream('win1250'))
			.pipe(iconv.encodeStream('utf8'))
			.pipe(parse({delimiter: ';'}))
			.on('data', (csvrow) => {			
				//do something with csvrow
				csvData.push(csvrow);        
			})
			.on('end',() => {
				//do something with csvData
				console.log('read '+infile);
				resolve(generateInserts(tablename,csvData));
			})
			.on('error',() => reject())
		}
	);
}

function runQueries(queries,con) {
	let promises=[];
	for (let i=0;i<queries.length;i++) {
		promises.push(query(queries[i],con));
	}
	let res=Promise.all(promises);
	return res;
}

function query(sql,con) {
	return new Promise( ( resolve, reject ) => {
        con.query( sql, ( err, rows ) => {
		console.log(sql);
        if ( err )
            return reject( err );
        resolve( rows );
        } );
    } );;
}

function clearDB(con) {
	let clearCmd=['TRUNCATE klassen',
		'TRUNCATE kurswahl',
		'TRUNCATE lehrer',
		'TRUNCATE schueler',
		'TRUNCATE sgruppen',
		'TRUNCATE unterricht'
	];
	return runQueries(clearCmd,con);
}

// ========== MAIN ===========

(async () => {
    // open the database
    const con = await open({
      filename: 'untis.db',
      driver: sqlite3.Database
    })

	console.log('opened, creating schema...');
	await clearDB(con);

	console.log('populating DB...');
	
	await convert('GPU004-Lehrer.TXT','lehrer',con)
	.then (ins => runQueries(ins,con));
	
	await convert('GPU002-Unterricht.TXT','unterricht',con)
	.then (ins => runQueries(ins,con));

	await convert('GPU003-Klassen.TXT','klassen',con)
	.then (ins => runQueries(ins,con));

	await convert('GPU010-Schüler.TXT','schueler',con)
	.then (ins => runQueries(ins,con));
	
	await convert('GPU015-Kurswahl.TXT','kurswahl',con)
	.then (ins => runQueries(ins,con));

	con.end();
	process.exit(0);
	
})()


