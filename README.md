# csv4iserv

This tool reads .CSV files exported by Untis and generates .CSV files to be imported by IServ.
It is written in Javascript and uses the NodeJS ecosystem.

**preparation**
1) Create a database in a local MySQL Server (e.g. use XAMPP).
2) Import database schema from csvimport.sql.
3) Change credentials in pinsert.js.

**usage**

1) Put GPUxxx\*.TXT files in "data_in".
2) Run **node pinsert.js**
