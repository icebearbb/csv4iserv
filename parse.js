var fs = require('fs'); 
var parse = require('csv-parse');
var iconv = require('iconv-lite');

// TRUNCATE klassen;TRUNCATE kurswahl;TRUNCATE lehrer;TRUNCATE schueler;TRUNCATE sgruppen; TRUNCATE unterricht;

// d:\xampp\mysql\bin\mysql.exe -u root untis21 --default-character-set=utf8 < data_out\kurswahl.sql

function generateInserts(tablename,csvdata) {
	let data="";
	for (let r in csvdata) {
		let row=csvdata[r];
		let ins='INSERT INTO '+tablename+' VALUES (';
		for (let cell in row) {
			ins+="'"+row[cell]+"',"
		}
		ins=ins.substring(0,ins.length-1)+");\r\n";
		data+=ins;
	}
	fs.writeFile('data_out/'+tablename+'.sql',data,'utf8',function (err) {
	  if (err) return console.log(err);
	  console.log('wrote '+tablename+'.sql');
	});
}

function convert(infile,tablename) {
	let csvData=[];
	fs.createReadStream('data_in/'+infile)
		.pipe(iconv.decodeStream('win1250'))
		.pipe(iconv.encodeStream('utf8'))
		.pipe(parse({delimiter: ';'}))
		.on('data', function(csvrow) {			
			//do something with csvrow
			csvData.push(csvrow);        
		})
		.on('end',function() {
		  //do something with csvData
		  console.log('read '+infile);
		  generateInserts(tablename,csvData);
		});
}

convert('GPU004-Lehrer.TXT','lehrer');
convert('GPU002-Unterricht.TXT','unterricht');
convert('GPU003-Klassen.TXT','klassen');
convert('GPU010-Schüler.TXT','schueler');
convert('GPU015-Kurswahl.TXT','kurswahl');